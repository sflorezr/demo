package com.solati.rest.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ObtenerSemana {
    public static void main(String[] args){
        retornaDia("2020-03-26");
        retornaDia("2020-03-31");
    }

    public static int retornaDia(String date){
        System.out.println(date);
        String fecha=date;
        String dia = "";
        String semana = null;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaActual = null;
        try {
            fechaActual = df.parse(fecha);
        } catch (ParseException e) {
            System.err.println("No se ha podido parsear la fecha.");
            e.printStackTrace();
        }
        GregorianCalendar fechaCalendario = new GregorianCalendar();
        fechaCalendario.setTime(fechaActual);
        int diaSemana = fechaCalendario.get(Calendar.WEEK_OF_MONTH);
        System.out.println(diaSemana);
        diaSemana = fechaCalendario.get(Calendar.DAY_OF_WEEK);
        System.out.println(diaSemana);
        dia = String.valueOf(diaSemana);
        int dia_devolver;
        if (dia.equals("1")){
            dia_devolver=7;
        }else{
            dia_devolver=Integer.parseInt(dia)-1;
        }
        System.out.println(dia_devolver);
        return dia_devolver;
    }
}
