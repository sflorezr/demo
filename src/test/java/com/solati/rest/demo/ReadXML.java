package com.solati.rest.demo;

import java.io.File;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ReadXML {
    public static void main(String[] args) {

        try {

            File file = new File("src/prueba2.xml");
            String contents = new String(Files.readAllBytes(Paths.get("src/prueba.xml")));
           // System.out.println("Contents (Java 7) : " + contents);
            DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            Document doc = dBuilder.parse(file);
          //  doc.getDocumentElement().normalize();
            //System.out.println("Root element :" + file);
           // NodeList nList = doc.getElementsByTagName("cbc:Description");
            printJSON(contents);

        /*    if (doc.hasChildNodes()) {

                printNote(doc.getChildNodes());

            }*/

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void printDocument(String ndoc) throws IOException {
        //Files.write( Paths.get("src/pruebadelaprueba.xml"), ndoc.getBytes());
        try {

            File file = new File("src/pruebadelaprueba.xml");

            DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            Document doc = dBuilder.parse(file);

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getChildNodes();

            if (doc.hasChildNodes()) {


                for (int count = 0; count < nList.getLength(); count++) {
                    Node tempNode = nList.item(0);

                    // make sure it's element node.
                    if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                        //  printNote(nList);
                        if (tempNode.hasAttributes()) {

                            // get attributes names and values
                            NamedNodeMap nodeMap = tempNode.getAttributes();

                            for (int i = 0; i < nodeMap.getLength(); i++) {

                                Node node = nodeMap.item(i);
                                System.out.println("attr name : " + node.getNodeName());
                                System.out.println("attr value : " + node.getNodeValue());


                            }

                        }
                    }

                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void printJSON(String xml) throws IOException {
        try {
            JSONObject jsonObj = XML.toJSONObject(xml);
            String json = jsonObj.toString(4);

            System.out.println(json);
            FileWriter myWriter = new FileWriter("src/prueba.json");
            myWriter.write(json);
            myWriter.close();
            ReadJson.LeerJson();
        } catch (JSONException je) {
            System.out.println(je.toString());
        }
    }
    private static void printNote(NodeList nodeList) throws IOException {

        for (int count = 0; count < nodeList.getLength(); count++) {

            Node tempNode = nodeList.item(count);

            // make sure it's element node.
            if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

                // get node name and value
                System.out.println("\nNode Name =" + tempNode.getNodeName() + " [OPEN]");
                System.out.println("Node Value =" + tempNode.getTextContent());
                System.out.println("por aqui pase");
             //   printJSON(tempNode.getTextContent());
             //   printDocument(tempNode.getTextContent());

               if (tempNode.hasAttributes()) {

                    // get attributes names and values
                    NamedNodeMap nodeMap = tempNode.getAttributes();

                    for (int i = 0; i < nodeMap.getLength(); i++) {

                        Node node = nodeMap.item(i);
                        System.out.println("attr name : " + node.getNodeName());
                        System.out.println("attr value : " + node.getNodeValue());


                    }

                }

                if (tempNode.hasChildNodes()) {
                    // loop again if has child nodes
                       printNote(tempNode.getChildNodes());
                }

                System.out.println("Node Name =" + tempNode.getNodeName() + " [CLOSE]");

            }

//        }
        }
    }
}
