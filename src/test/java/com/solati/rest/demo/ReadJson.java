package com.solati.rest.demo;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.List;

public class ReadJson {
    public static void main(String[] args) {
        JSONParser parser = new JSONParser();
        try{
        Object objeto = parser.parse(new FileReader("src/prueba2.json"));
        JSONObject obj = new JSONObject(objeto.toString());
        JSONArray objetos = obj.getJSONObject("Invoice").getJSONArray("cac:InvoiceLine");

        //JSONArray arr = obj.getJSONArray("posts");
        for (int i = 0; i < objetos.length(); i++) {
            String post_id =  objetos.getJSONObject(i).get("cac:Price").toString();
            System.out.println("--------------------------------------------------------");
           // System.out.println("Identificador: "+post_id);
            System.out.println("precio Neto: "+objetos.getJSONObject(i).getJSONObject("cac:Price").getJSONObject("cbc:PriceAmount").get("content").toString());
            System.out.println("codigo: "+objetos.getJSONObject(i).getJSONObject("cac:Item").getJSONObject("cac:SellersItemIdentification").get("cbc:ID").toString());
            System.out.println("Cantidad: "+objetos.getJSONObject(i).getJSONObject("cac:Price").getJSONObject("cbc:BaseQuantity").get("content").toString());
            System.out.println("precio con descuento: "+objetos.getJSONObject(i).getJSONObject("cbc:LineExtensionAmount").get("content").toString());
            try{
                System.out.println("Porcentaje descuento: "+objetos.getJSONObject(i).getJSONObject("cac:AllowanceCharge").get("cbc:MultiplierFactorNumeric").toString());
            }catch (Exception e){

            }
            System.out.println("--------------------------------------------------------");
        }
     }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void LeerJson(){
        JSONParser parser = new JSONParser();
        try{
            Object objeto = parser.parse(new FileReader("src/prueba2.json"));
            JSONObject obj = new JSONObject(objeto.toString());
            JSONArray objetos = obj.getJSONObject("Invoice").getJSONArray("cac:InvoiceLine");

            //JSONArray arr = obj.getJSONArray("posts");
            for (int i = 0; i < objetos.length(); i++) {
                String post_id =  objetos.getJSONObject(i).get("cac:Price").toString();
                System.out.println("--------------------------------------------------------");
                // System.out.println("Identificador: "+post_id);
                System.out.println("precio Neto: "+objetos.getJSONObject(i).getJSONObject("cac:Price").getJSONObject("cbc:PriceAmount").get("content").toString());
                System.out.println("codigo: "+objetos.getJSONObject(i).getJSONObject("cac:Item").getJSONObject("cac:SellersItemIdentification").get("cbc:ID").toString());
                System.out.println("Cantidad: "+objetos.getJSONObject(i).getJSONObject("cac:Price").getJSONObject("cbc:BaseQuantity").get("content").toString());
                System.out.println("precio con descuento: "+objetos.getJSONObject(i).getJSONObject("cbc:LineExtensionAmount").get("content").toString());
                // System.out.println("Porcentaje descuento: "+objetos.getJSONObject(i).getJSONObject("cac:AllowanceCharge").get("cbc:MultiplierFactorNumeric").toString());
                System.out.println("--------------------------------------------------------");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
